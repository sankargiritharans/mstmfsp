trigger ContactTrigger_AT on Contact (after insert) {
    
    if(Trigger.isAfter && Trigger.isInsert){
        Set<Id> accountIdSet = new Set<Id>();
        Map<Id,Account> accountMap = new Map<Id,Account>();
        List<Opportunity> newOppList = new List<Opportunity>();
        for(Contact processContact : trigger.new){
            
            accountIdSet.add(processContact.accountId);
            
        }
        
        if(accountIdSet.size() > 0){
            for(Account dbAccount : [SELECT id,Sic FROM Account WHERE Id IN:accountIdSet]){
                accountMap.put(dbAccount.Id,dbAccount);
            }
        }
        if(accountMap.size() > 0){
          
          for(Contact processContact : trigger.new){
             
                 if(accountMap.containsKey(processContact.AccountId)){
                     
                     Opportunity newOppRec = new Opportunity();
                     newOppRec.AccountId = processContact.AccountId;
                     newOppRec.Contact__c = processContact.Id;
                     newOppRec.OrderNumber__c = accountMap.get(processContact.AccountId).Sic;
                     newOppRec.CloseDate = system.today() + 5;
                     newOppRec.Name = 'Test Code';
                     newOppRec.StageName = 'Prospecting';
                     newOppList.add(newOppRec);
                 }
            
         }
        }
        if(newOppList.size() > 0)
        insert newOppList;
    }

}