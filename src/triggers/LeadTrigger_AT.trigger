trigger LeadTrigger_AT on Lead (before insert, before update) {
	Set<String> newEmail = new Set<String>();
    Set<String> ExitEmail = new Set<String>();
    
    for(Lead leadRec: Trigger.New) {
        If((leadRec.Email != NULL && leadRec.Email != '') && (Trigger.isInsert || leadRec.Email != Trigger.oldMap.get(leadRec.Id).Email)) {
            If(!newEmail.contains(leadRec.Email)) {
                 newEmail.add(leadRec.Email);
            }
        }
    }
    
    If(newEmail.size() > 0) {
        for(Lead dbLeadRec:[SELECT Email FROM Lead WHERE Email IN:newEmail]) {
            ExitEmail.add(dbLeadRec.Email);
        }
    }
    
    for(Lead checkLead: Trigger.new) {
        System.Debug(ExitEmail);
        If(ExitEmail.contains(checkLead.Email)) {
            checkLead.Email.addError('Email Already Exist:' + checkLead.Email); 
        } else {
            ExitEmail.add(checkLead.Email);
        }
    }    
}