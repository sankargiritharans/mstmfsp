trigger AccountTrigger_AT on Account (before insert,before update,after insert,after update) {
    AccountTriggerHandler accountIns = new AccountTriggerHandler();
    if(Trigger.isBefore && Trigger.isInsert ){
        Set<String> emailSet = new Set<String>();
        Set<String> dbEmailSet = new Set<String>();
        for(Account newAccount : trigger.new){
            
            emailSet.add(newAccount.Phone);
        }
        if(emailSet.size() > 0){
            
            for(Account dbAccount : [SELECT Id,Name,Phone FROM Account WHERE Phone IN:emailSet]){
            dbEmailSet.add(dbAccount.Phone);
            }
            
            for(Account newAccount : trigger.new){
                if(dbEmailSet.contains(newAccount.Phone)){
                    newAccount.addError('Phone Already exists');
                }
            }
        }
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
        Set<String> phoneSet = new Set<String> ();
        Set<String> dbphoneSet = new Set<String> ();
        for(Account updateAcc : trigger.new){
            
            if(updateAcc.Phone != trigger.oldMap.get(updateAcc.Id).Phone){
                phoneSet.add(updateAcc.Phone);
            }
        }
        
       if(phoneSet.size() > 0){
            
            for(Account dbAccount : [SELECT Id,Name,Phone FROM Account WHERE Phone IN: phoneSet]){
            dbphoneSet.add(dbAccount.Phone);
            }
            
            for(Account newAccount : trigger.new){
                if(dbphoneSet.contains(newAccount.Phone)){
                    newAccount.addError('Phone Already exists');
                }
            }
        }
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        Map<Id,String> accountMap = new Map<Id,String>();
        
        for(Account dbAccount : trigger.new){
            if(dbAccount.billingStreet != trigger.oldMap.get(dbAccount.Id).billingStreet){
                accountMap.put(dbAccount.Id,dbAccount.billingStreet);
            }
        }
        
        if(accountMap.size() > 0){
        
        accountIns.onAfterUpdate(accountMap);
        }
    }
    
    if(Trigger.isAfter && Trigger.isInsert){
        List<Account > accountList = new List<Account >();
        for(Account newAccount : trigger.new){
            accountList.add(newAccount);
        }
        if(accountList.size() > 0){
       
        accountIns.onAfterInsert(accountList);
        }
     }
     
     
   }