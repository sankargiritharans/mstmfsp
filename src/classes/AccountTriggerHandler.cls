Public class AccountTriggerHandler{

    public void onAfterInsert(List<Account> accList){
        List<Contact> conList = new List<Contact>();
        for(Account processAccRec : accList){
            
            Contact newConRec = new Contact();
            newConRec.AccountId = processAccRec.Id;
            newConRec.LastName = 'Test';
            conList.add(newConRec);
        }
        
        if(conList.size() > 0)
        insert conList;
     
    }
    
    public void onAfterUpdate(Map<Id,String> accMap){
        List<Contact> updateList = new List<Contact>();
        for(Contact dbCon : [SELECT Id,AccountId,MailingStreet FROM Contact WHERE AccountId IN: accMap.KeySet()]){
            
            if(accMap.containsKey(dbCon.AccountId)){
            dbCon.MailingStreet  = accMap.get(dbCon.AccountId);
            updateList.add(dbCon);
            }
        }
        
        if(updateList.size() > 0)
        update updateList;
    }

}