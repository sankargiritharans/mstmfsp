public class AutoLeadConvert_AC {
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
        LeadStatus CurLeadStatus= [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];
        Set<Id> accId = new Set<Id>();
        Set<String> existAccName = new Set<String>();
        Set<Id> leadId = new Set<Id>();
        //Set<String> leadCompany = new Set<String>();
        List<String> leadCompany = new List<String>();
        List<Database.LeadConvert> MassLeadconvert = new List<Database.LeadConvert>();
        List<Account> updateList = new List<Account>();
        for(id currentlead: LeadIds) {
                Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(currentlead);
            	leadId.add(currentlead);
                Leadconvert.setConvertedStatus(CurLeadStatus.MasterLabel);  
                MassLeadconvert.add(Leadconvert);
        }
        
        if (!MassLeadconvert.isEmpty()) {
            List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
             
            for(Database.LeadConvertResult convertLeads :lcr){
                //leadContactMap.put(convertLeads.getLeadId(),convertLeads.getContactId());
                accId.add(convertLeads.getAccountId());
                //conId.add(convertLeads.getContactId());
                leadId.add(convertLeads.getLeadId());
            }
            
        	if(accId.size() > 0 && leadId.size() > 0) {
               for(Lead newLeadRec : [SELECT Id,Company FROM Lead WHERE Id IN: leadId]) {
                   leadCompany.add(newLeadRec.Company);
               }
                
                for(Account dbAccRec : [SELECT Id,Name FROM Account WHERE Id IN: accId]) {
                    dbAccRec.Name = leadCompany[0];
                    updateList.add(dbAccRec);
               	}
                
                if(updateList.size() > 0)
                	update updateList;
            }     
        }
    }
}